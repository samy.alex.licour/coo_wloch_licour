package fil.coo.TP0;

/**
 * The Student class is a class of student.
 *@author Wloch Paul Licour Samy
 */
public class Student 
{
	/**
	 * a String to give a  name to the student
	 * a String to give a number to the student
	 */
	private String name;
	private String studentNumber;

	/**
	  * Constructor.
	  * @param name (required)
	  * @param studentNumber (required) .
	  */
	
	public Student(String name, String studentNumber) {
		this.name = name;
		this.studentNumber = studentNumber;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getStudentNumber() {
		return studentNumber;
	}
	public void setStudentNumber(String studentNumber) {
		this.studentNumber = studentNumber;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Student other = (Student) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (studentNumber != other.studentNumber)
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "Student [name=" + name + ", studentNumber=" + studentNumber + "]";
	}

}
