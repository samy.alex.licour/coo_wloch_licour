package fil.coo.TP0;

import static org.junit.Assert.*;

import org.junit.Test;

public class StudentTest {
	private Student student;
	
	
	@Test
	public void testSetName() {
		student= new Student("paul","rz");
		student.setName("namefd");
		assertEquals("namefd",student.getName());
	}
	
	@Test
	public void testSetStudentNumbe() {
		student= new Student("paul","rz");
		student.setStudentNumber("namefd");
		assertEquals("namefd",student.getStudentNumber());
	}
	
	@Test
	public void testGetName() {
		student= new Student("paul","rz");
		assertEquals("paul",student.getName());
	}
	
	@Test
	public void testGetNumber() {
		student= new Student("paul","rz");
		assertEquals("rz",student.getStudentNumber());
	}

}
